@extends('layouts.main')

@section('title', ' - start page')
    
@section('main')
    @include('layouts.main_elements.header', ['title' => 'Payments: start Page'])
@endsection
