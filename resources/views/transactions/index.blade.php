@extends('layouts.main')

@section('main')

    @include('layouts.main_elements.header', ['title' => 'Transactions'])

    <div class="container">
        <h3>Transactions</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">customer_id</th>
                    <th scope="col">product</th>
                    <th scope="col">amount</th>
                    <th scope="col">currency</th>
                    <th scope="col">status</th>
                    <th scope="col">created_at</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($payments as $transaction)
                <tr>
                    <td scope="row">{{ $transaction->id }}</td>
                    <td>{{ $transaction->customer_id }}</td>
                    <td>{{ $transaction->product }}</td>
                    <td>{{ $transaction->amount }}</td>
                    <td>{{ $transaction->currency }}</td>
                    <td>{{ $transaction->status }}</td>
                    <td>{{ $transaction->created_at }}</td>
                </tr>
                @empty
                    @include('layouts.main_elements.table_empty_data')
                @endforelse
            </tbody>
        </table>
    </div>
    
@endsection