@extends('layouts.main')

@section('title', ' - start page')
    
@section('main')
    @include('layouts.main_elements.header', ['title' => 'Pay Page'])
    
    @include('layouts.site_elements.flash_messages')
    
    @include('layouts.site_elements.form_errors')

    <div class="container py-2">
        <form action="{{ route('web.transactions.store') }}" method="post" id="payment-form">
            @method('POST')
            @csrf
            <div class="customer-data mb-3">
                <div class="card">
                    <div class="card-header">
                        <p class="lead mb-0">Payment data</p>
                    </div>
                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="description" class="col-sm-2 col-form-label">Description:</label>
                            <div class="col-sm-10">
                                <input type="text" name="description" id="description" class="form-control" value="Intro To Stripe Course">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <label for="amount" class="col-sm-4 col-form-label">Amount:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="amount" id="amount" class="form-control mb-3" value="99" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <label for="currency" class="col-sm-4 col-form-label">Currency:</label>
                                    <div class="col-sm-8">
                                        <select class="form-select" name="currency" id="currency" aria-label="Default select example">
                                            <option value="usd" selected>usd</option>
                                            <option value="pln">PLN</option>
                                            <option value="gb">gb</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <p class="lead mb-0">Customer data</p>
                </div>
                <div class="card-body">

                    <div class="row g-3">
                        <div class="col">
                            <input type="text" name="first_name" value="{{ old('first_name') ?? '' }}" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="First Name" required>
                        </div>
                        <div class="col">
                            <input type="text" name="last_name" value="{{ old('last_name') ?? '' }}" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col">
                            <input type="email" name="email" value="{{ old('email') ?? '' }}" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="Email Address">
                        </div>
                        <div class="col">
                            <div id="card-element" class="form-control">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <div id="cardHelp" class="form-text">
                                <a href="https://stripe.com/docs/terminal/references/testing#standard-test-cards">Card numbers</a>.
                                <br><span>4242424242424242</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Used to display form errors -->
            <div id="card-errors" role="alert"></div>

            <button type="submit">Submit Payment</button>
        </form>
    </div>

    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ asset('js/charge.js') }}"></script>


@endsection
