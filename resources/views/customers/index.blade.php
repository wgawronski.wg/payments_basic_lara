@extends('layouts.main')

@section('main')

    @include('layouts.main_elements.header', ['title' => 'Customers'])

    <div class="container">
        <h3>Customers</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">full name</th>
                    <th scope="col">email</th>
                    <th scope="col">created_at</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($customers as $customer)
                <tr>
                    <td scope="row">{{ $customer->id}}</td>
                    <td>{{ $customer->first_name }} {{ $customer->first_name }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->created_at }}</td>
                </tr>
                @empty
                    @include('layouts.main_elements.table_empty_data')
                @endforelse
            </tbody>
        </table>
    </div>
    
@endsection