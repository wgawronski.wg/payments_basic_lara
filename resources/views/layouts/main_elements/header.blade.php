<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="display-4 my-3">{{ $title }}</h1>
            <p class="lead">{{ $subTitle ?? '' }}</p>
        </div>
    </div>
</div>