<div class="container">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('web.start') }}">Stripe</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav me-auto">
                <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.start') }}">start</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.payments.index') }}">payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.transactions.index') }}">transactions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.customers.index') }}">customers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.transactions.create') }}">pay page / transactions</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                <li>
                    <span>| {{ env('DB_CONNECTION') }}</span>
                </li>
                </ul>
            </div>
        </div>
    </nav>
</div>