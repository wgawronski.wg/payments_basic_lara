<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionPostRequest;
use App\Models\Customer;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TransactionController extends Controller
{
    public function index(): View
    {
        $payments = Transaction::all();
        return view('transactions.index', compact('payments'));
    }

    public function create()
    {
        return view('transactions.create');
    }

    public function store(TransactionPostRequest $request)
    {
        $validated = (object) $request->validated();
        dd($validated);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // Create Customer In Stripe
        $customer = \Stripe\Customer::create(array(
            "email" => $validated->email,
            "source" => $validated->stripeToken
        ));

        $charge = \Stripe\Charge::create(array(
            "amount" => $validated->amount,
            "currency" => $validated->currency,
            "description" => $validated->description,
            "customer" => $customer->id
          ));

        $customerData = [
            'id' => $charge->customer,
            'first_name' => $validated->first_name,
            'last_name' => $validated->last_name,
            'email' => $validated->email
          ];

          Customer::created();

          $transactionData = [
            'id' => $charge->id,
            'customer_id' => $charge->customer,
            'product' => $charge->description,
            'amount' => $charge->amount,
            'currency' => $charge->currency,
            'status' => $charge->status,
          ];

        Stripe\Charge::create ([
                "amount" => $validated->amouunt * 100,
                "currency" => $validated->currency,
                "source" => $request->stripeToken,
                "description" => $validated->description
        ]);
   
        return back()->with('success', 'Payment successful!');
    }

}
