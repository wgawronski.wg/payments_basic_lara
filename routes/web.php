<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'app')->name('web.start');


// Route::view('/pay-page', 'pay_page')->name('web.pay_page');
Route::get('/transactions/create', [TransactionController::class, 'create'])->name('web.transactions.create');
// Route::get('stripe', [StripeController::class, 'stripe']);
// Route::post('pay-page', [StripeController::class, 'stripePost'])->name('stripe.post');
Route::post('/transactions', [TransactionController::class, 'store'])->name('web.transactions.store');
Route::get('/transactions', [TransactionController::class, 'index'])->name('web.transactions.index');


Route::get('/payments', [PaymentController::class, 'index'])->name('web.payments.index');
Route::get('/customers', [CustomerController::class, 'index'])->name('web.customers.index');
