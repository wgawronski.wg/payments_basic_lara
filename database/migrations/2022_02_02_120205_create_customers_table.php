<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *   CREATE TABLE `customers` (
     *    `id` varchar(255) NOT NULL,
     *    `first_name` varchar(255) NOT NULL,
     *    `last_name` varchar(255) NOT NULL,
     *    `email` varchar(255) NOT NULL,
     *    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
     *    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('email', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
