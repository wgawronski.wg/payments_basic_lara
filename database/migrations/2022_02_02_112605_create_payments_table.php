<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     * CREATE TABLE `transactions` (
     *   `id` varchar(255) NOT NULL,
     *   `customer_id` varchar(255) NOT NULL,
     *   `product` varchar(255) NOT NULL,
     *   `amount` varchar(255) NOT NULL,
     *   `currency` varchar(255) NOT NULL,
     *   `status` varchar(255) NOT NULL,
     *   `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
     *   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->string('product', 255);
            $table->string('amount', 255);
            $table->string('currency', 255);
            $table->string('status', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
