Instalacja projektu. Główne kroki:

```
composer install  
touch database/database.sqlite  
cp .env.example .env
php artisan key:generate
DB_CONNECTION=sqlite
php artisan migrate  
php aritsan serve  
```

- Repozytoria payments_basic_lara i payments_basic_php korzystają z tej samej bazy MySQL.  
- Lara dodatkowo z SQLite, co może (ale nie musi) być traktowane jako lokalna BD.

---

```
composer require stripe/stripe-php

```
przykładowa implementacja:
https://hackthestuff.com/article/laravel-8-how-to-integrate-stripe-payment-gateway-api-with-example